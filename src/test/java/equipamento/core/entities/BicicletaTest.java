package equipamento.core.entities;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BicicletaTest {
    @Test
    void newBicicleta() {
        String marca = "marca1";
        String modelo = "modelo1";
        String ano = "ano1";
        int numero = 1;
        Bicicleta.Status status = Bicicleta.Status.DISPONIVEL;
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), marca, modelo, ano, numero, status);

        assertNotNull(bicicleta.getId());
        assertEquals(bicicleta.getMarca(), marca);
        assertEquals(bicicleta.getModelo(), modelo);
        assertEquals(bicicleta.getAno(), ano);
        assertEquals(bicicleta.getNumero(), numero);
        assertEquals(bicicleta.getStatus(), status);

        bicicleta.setId(UUID.fromString("00000000-0000-0000-0000-000000000000"));
        assertEquals(bicicleta.getId(), UUID.fromString("00000000-0000-0000-0000-000000000000"));
    }

    @Test
    void getId() {
        UUID id = UUID.randomUUID();
        Bicicleta bicicleta = new Bicicleta(id, "marca1", "modelo1", "ano1", 1, Bicicleta.Status.DISPONIVEL);

        assertEquals(bicicleta.getId(), id);
    }

    @Test
    void getMarca() {
        String marca = "marca1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), marca, "modelo1", "ano1", 1,
                Bicicleta.Status.DISPONIVEL);

        assertEquals(bicicleta.getMarca(), marca);
    }

    @Test
    void setMarca() {
        String marca = "marca1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), marca, "modelo1", "ano1", 1,
                Bicicleta.Status.DISPONIVEL);
        String marca2 = "marca2";
        bicicleta.setMarca(marca2);

        assertEquals(bicicleta.getMarca(), marca2);
    }

    @Test
    void getModelo() {
        String modelo = "modelo1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", modelo, "ano1", 1,
                Bicicleta.Status.DISPONIVEL);

        assertEquals(bicicleta.getModelo(), modelo);
    }

    @Test
    void setModelo() {
        String modelo = "modelo1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", modelo, "ano1", 1,
                Bicicleta.Status.DISPONIVEL);
        String modelo2 = "modelo2";
        bicicleta.setModelo(modelo2);

        assertEquals(bicicleta.getModelo(), modelo2);
    }

    @Test
    void getAno() {
        String ano = "ano1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", ano, 1,
                Bicicleta.Status.DISPONIVEL);

        assertEquals(bicicleta.getAno(), ano);
    }

    @Test
    void setAno() {
        String ano = "ano1";
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", ano, 1,
                Bicicleta.Status.DISPONIVEL);
        String ano2 = "ano2";
        bicicleta.setAno(ano2);

        assertEquals(bicicleta.getAno(), ano2);
    }

    @Test
    void getNumero() {
        int numero = 1;
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", "ano1", numero,
                Bicicleta.Status.DISPONIVEL);

        assertEquals(bicicleta.getNumero(), numero);
    }

    @Test
    void setNumero() {
        int numero = 1;
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", "ano1", numero,
                Bicicleta.Status.DISPONIVEL);
        int numero2 = 2;
        bicicleta.setNumero(numero2);

        assertEquals(bicicleta.getNumero(), numero2);
    }

    @Test
    void getStatus() {
        Bicicleta.Status status = Bicicleta.Status.DISPONIVEL;
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", "ano1", 1, status);

        assertEquals(bicicleta.getStatus(), status);
    }

    @Test
    void setStatus() {
        Bicicleta.Status status = Bicicleta.Status.DISPONIVEL;
        Bicicleta bicicleta = new Bicicleta(UUID.randomUUID(), "marca1", "modelo1", "ano1", 1, status);
        Bicicleta.Status status2 = Bicicleta.Status.EM_REPARO;
        bicicleta.setStatus(status2);

        assertEquals(bicicleta.getStatus(), status2);
    }

}
