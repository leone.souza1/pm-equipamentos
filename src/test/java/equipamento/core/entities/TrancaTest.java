package equipamento.core.entities;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class TrancaTest {
    @Test
    void newTranca() {
        UUID bicicleta = UUID.randomUUID();
        Integer numero = 1;
        String localizacao = "localizacao1";
        String anoDeFabricacao = "2020";
        String modelo = "modelo1";
        Tranca.Status status = Tranca.Status.LIVRE;
        Tranca tranca = new Tranca(UUID.randomUUID(), bicicleta, numero, localizacao, anoDeFabricacao, modelo, status);

        assertNotNull(tranca.getId());
        assertEquals(tranca.getBicicleta(), bicicleta);
        assertEquals(tranca.getNumero(), numero);
        assertEquals(tranca.getLocalizacao(), localizacao);
        assertEquals(tranca.getAnoDeFabricacao(), anoDeFabricacao);
        assertEquals(tranca.getModelo(), modelo);
        assertEquals(tranca.getStatus(), status);
    }

    @Test
    void getId() {
        UUID id = UUID.randomUUID();
        Tranca tranca = new Tranca(id, UUID.randomUUID(), 1, "localizacao1", "2020", "modelo1", Tranca.Status.LIVRE);

        assertEquals(tranca.getId(), id);
    }

    @Test
    void getBicicleta() {
        UUID bicicleta = UUID.randomUUID();
        Tranca tranca = new Tranca(UUID.randomUUID(), bicicleta, 1, "localizacao1", "2020", "modelo1",
                Tranca.Status.LIVRE);

        assertEquals(tranca.getBicicleta(), bicicleta);
    }

    @Test
    void setBicicleta() {
        UUID bicicleta = UUID.randomUUID();
        Tranca tranca = new Tranca(UUID.randomUUID(), bicicleta, 1, "localizacao1", "2020", "modelo1",
                Tranca.Status.LIVRE);
        UUID bicicleta2 = UUID.randomUUID();
        tranca.setBicicleta(bicicleta2);

        assertEquals(tranca.getBicicleta(), bicicleta2);
    }

    @Test
    void getNumero() {
        Integer numero = 1;
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), numero, "localizacao1", "2020", "modelo1",
                Tranca.Status.LIVRE);

        assertEquals(tranca.getNumero(), numero);
    }

    @Test
    void setNumero() {
        Integer numero = 1;
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), numero, "localizacao1", "2020", "modelo1",
                Tranca.Status.LIVRE);
        Integer numero2 = 2;
        tranca.setNumero(numero2);

        assertEquals(tranca.getNumero(), numero2);
    }

    @Test
    void getLocalizacao() {
        String localizacao = "localizacao1";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, localizacao, "2020", "modelo1",
                Tranca.Status.LIVRE);

        assertEquals(tranca.getLocalizacao(), localizacao);
    }

    @Test
    void setLocalizacao() {
        String localizacao = "localizacao1";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, localizacao, "2020", "modelo1",
                Tranca.Status.LIVRE);
        String localizacao2 = "localizacao2";
        tranca.setLocalizacao(localizacao2);

        assertEquals(tranca.getLocalizacao(), localizacao2);
    }

    @Test
    void getAnoDeFabricacao() {
        String anoDeFabricacao = "2020";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", anoDeFabricacao, "modelo1",
                Tranca.Status.LIVRE);

        assertEquals(tranca.getAnoDeFabricacao(), anoDeFabricacao);
    }

    @Test
    void setAnoDeFabricacao() {
        String anoDeFabricacao = "2020";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", anoDeFabricacao, "modelo1",
                Tranca.Status.LIVRE);
        String anoDeFabricacao2 = "2021";
        tranca.setAnoDeFabricacao(anoDeFabricacao2);

        assertEquals(tranca.getAnoDeFabricacao(), anoDeFabricacao2);
    }

    @Test
    void getModelo() {
        String modelo = "modelo1";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", "2020", modelo,
                Tranca.Status.LIVRE);

        assertEquals(tranca.getModelo(), modelo);
    }

    @Test
    void setModelo() {
        String modelo = "modelo1";
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", "2020", modelo,
                Tranca.Status.LIVRE);
        String modelo2 = "modelo2";
        tranca.setModelo(modelo2);

        assertEquals(tranca.getModelo(), modelo2);
    }

    @Test
    void getStatus() {
        Tranca.Status status = Tranca.Status.LIVRE;
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", "2020", "modelo1", status);

        assertEquals(tranca.getStatus(), status);
    }

    @Test
    void setStatus() {
        Tranca.Status status = Tranca.Status.LIVRE;
        Tranca tranca = new Tranca(UUID.randomUUID(), UUID.randomUUID(), 1, "localizacao1", "2020", "modelo1", status);
        Tranca.Status status2 = Tranca.Status.OCUPADA;
        tranca.setStatus(status2);

        assertEquals(tranca.getStatus(), status2);
    }

}
