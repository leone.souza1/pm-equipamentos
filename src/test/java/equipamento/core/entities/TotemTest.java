package equipamento.core.entities;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/*Totem
 * public class Totem {
    private UUID id;
    private String localizacao;
}
 */

class TotemTest {
    @Test
    void newTotem() {
        String localizacao = "localizacao1";
        Totem totem = new Totem(UUID.randomUUID(), localizacao);

        assertNotNull(totem.getId());
        assertEquals(totem.getLocalizacao(), localizacao);
    }

    @Test
    void getId() {
        UUID id = UUID.randomUUID();
        Totem totem = new Totem(id, "localizacao1");

        assertEquals(totem.getId(), id);
    }

    @Test
    void getLocalizacao() {
        String localizacao = "localizacao1";
        Totem totem = new Totem(UUID.randomUUID(), localizacao);

        assertEquals(totem.getLocalizacao(), localizacao);
    }

    @Test
    void setLocalizacao() {
        String localizacao = "localizacao1";
        Totem totem = new Totem(UUID.randomUUID(), localizacao);
        String localizacao2 = "localizacao2";
        totem.setLocalizacao(localizacao2);

        assertEquals(totem.getLocalizacao(), localizacao2);
    }
}
