package equipamento.core.repositories;

import equipamento.core.entities.Bicicleta;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
class BicicletaRepositoryTest {

        @Test
        void completeCrudTest(){
            BicicletaRepository cr = new BicicletaRepository();
            Bicicleta bicicleta = new Bicicleta();
            bicicleta.setId(UUID.randomUUID());
            bicicleta.setMarca("test");
            bicicleta.setModelo("test");
            bicicleta.setAno("test");
            bicicleta.setNumero(1);
            bicicleta.setStatus(Bicicleta.Status.DISPONIVEL);

            // SAVE
            cr.save(bicicleta);

            // GET BY ID | GET ALL
            Optional<Bicicleta> bOne = cr.findById(bicicleta.getId());
            Collection<Bicicleta> bicicletas = cr.findAll();

            assertTrue(bOne.isPresent());
            assertFalse(bicicletas.isEmpty());

            // UPDATE
            cr.setStatus(bicicleta, Bicicleta.Status.EM_USO);
            Optional<Bicicleta> bOneUpdated = cr.findById(bicicleta.getId());

            assertTrue(bOneUpdated.isPresent());
            assertEquals(Bicicleta.Status.EM_USO, bOneUpdated.get().getStatus());

            // DELETE
            cr.delete(bicicleta.getId());
            Optional<Bicicleta> bOneDeleted = cr.findById(bicicleta.getId());
            assertFalse(bOneDeleted.isPresent());
        }
}