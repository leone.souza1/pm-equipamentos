package equipamento.api.handlers;

import equipamento.api.JavalinApp;
import equipamento.api.requests.IntegrateBicicleta;
import equipamento.core.entities.Bicicleta;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class BicicletaControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7011;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getTests() {
        class getTest {
            public final String path;
            public final int returnCode;

            getTest(String path, int code) {
                this.path = path;
                this.returnCode = code;
            }
        }
        getTest t1 = new getTest("/bicicleta", 200);
        getTest t2 = new getTest("/bicicleta/00000000-0000-0000-0000-000000000011", 200);
        getTest t3 = new getTest("/bicicleta/00000000-0000-0000-0000-000000000900", 404);

        List<getTest> testList = Arrays.asList(t1, t2, t3);
        for (getTest test : testList) {
            final HttpResponse response = Unirest.get(baseUrl + test.path).asString();
            assertEquals(test.returnCode, response.getStatus());
            assertNotNull(response.getBody());
        }

    }

    @Test
    void postBicicleta_sucess() {
        Bicicleta b = new Bicicleta();
        b.setMarca("marcaTeste");
        b.setModelo("modeloTeste");
        b.setAno("anoTeste");
        b.setNumero(1);
        b.setStatus(Bicicleta.Status.DISPONIVEL);

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta").body(b).asString();
        assertEquals(201, response.getStatus());
    }

    @Test
    void postBicicleta_fail(){
        String body = "'{\n" +
                "  \"marca\": \"marcaTeste\",\n" +
                "  \"modelo\": \"modeloTeste\",\n" +
                "  \"ano\": \"anoTeste\",\n" +
                "  \"numero\": \"a\",\n" +
                "  \"status\": \"DISPONIVEL\"\n" +
                "}'";

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta").body(body).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void postBicicleta_IntegrarNaRede_fail() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000002");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000011");

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/integrarNaRede").body(b).asString();
        assertEquals(422, response.getStatus());
    }
    @Test
    void postBicicleta_IntegrarNaRede_sucess() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000004");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000012");

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/integrarNaRede").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void retirarDaRede_success() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000001");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000011");

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/retirarDaRede").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void retirarDaRede_404_fail() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000091");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000011");

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/retirarDaRede").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void retirarDaRede_422_fail() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000001");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000010");

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/retirarDaRede").body(b).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void retirarDaRede_sucess() {
        IntegrateBicicleta b = new IntegrateBicicleta();
        b.setIdTranca("00000000-0000-0000-0000-000000000005");
        b.setIdBicicleta("00000000-0000-0000-0000-000000000015");
        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/retirarDaRede").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void alterarStatus_sucess(){
        String idBicicleta = "00000000-0000-0000-0000-000000000012";
        String acao = "DISPONIVEL";

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/" + idBicicleta + "/status/" + acao).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void alterarStatus_404_fail(){
        String idBicicleta = "00000000-0000-0000-0000-000000000912";
        String acao = "DISPONIVEL";

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/" + idBicicleta + "/status/" + acao).asString();
        assertEquals(404, response.getStatus());
    }
    @Test
    void alterarStatus_422_fail(){
        String idBicicleta = "00000000-0000-0000-0000-000000000012";
        String acao = "A";

        final HttpResponse response = Unirest.post(baseUrl + "/bicicleta/" + idBicicleta + "/status/" + acao).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void putBicicleta_idBicicleta_sucess(){
        Bicicleta b = new Bicicleta();
        b.setMarca("marcaTeste");
        b.setModelo("modeloTeste");
        b.setAno("anoTeste");
        b.setNumero(1);
        b.setStatus(Bicicleta.Status.DISPONIVEL);

        final HttpResponse response = Unirest.put(baseUrl + "/bicicleta/00000000-0000-0000-0000-000000000012").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void putBicicleta_idBicicleta_404(){
        Bicicleta b = new Bicicleta();
        b.setMarca("marcaTeste");
        b.setModelo("modeloTeste");
        b.setAno("anoTeste");
        b.setNumero(1);
        b.setStatus(Bicicleta.Status.DISPONIVEL);

        final HttpResponse response = Unirest.put(baseUrl + "/bicicleta/00000000-0000-0000-0000-000000000912").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void putBicicleta_idBicicleta_422(){
        String body = "'{\n" +
                "  \"marca\": \"marcaTeste\",\n" +
                "  \"modelo\": \"modeloTeste\",\n" +
                "  \"ano\": \"anoTeste\",\n" +
                "  \"numero\": \"1\",\n" +
                "  \"status\": \"A\"\n" +
                "}'";

        final HttpResponse response = Unirest.put(baseUrl + "/bicicleta/00000000-0000-0000-0000-000000000012").body(body).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void deleteBicicleta_sucess(){
        final HttpResponse response = Unirest.delete(baseUrl + "/bicicleta/00000000-0000-0000-0000-000000000010").asString();
        assertEquals(204, response.getStatus());
    }

    @Test
    void deleteBicicleta_fail(){
        final HttpResponse response = Unirest.delete(baseUrl + "/bicicleta/00000000-0000-0000-0000-000000000010").asString();
        assertEquals(404, response.getStatus());
    }
}