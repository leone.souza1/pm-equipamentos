package equipamento.api.handlers;

import equipamento.api.JavalinApp;
import equipamento.api.requests.TrancaTotem;
import equipamento.core.entities.Tranca;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class TrancaControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7013;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }

    @AfterAll
    static void afterAll() {
        app.stop();
    }

    @Test
    void getTests() {
        class getTest {
            public final String path;
            public final int returnCode;

            getTest(String path, int code) {
                this.path = path;
                this.returnCode = code;
            }
        }
        getTest t1 = new getTest("/tranca", 200);
        getTest t2 = new getTest("/tranca/00000000-0000-0000-0000-000000000000", 200);
        getTest t3 = new getTest("/tranca/A", 404);
        getTest t4 = new getTest("/tranca/00000000-0000-0000-0000-000000000003/bicicleta", 200);
        getTest t5 = new getTest("/tranca/00000000-0000-0000-0000-000000000000/bicicleta", 404);
        getTest t6 = new getTest("/tranca/A/bicicleta", 404);

        List<getTest> testList = Arrays.asList(t1, t2, t3, t4, t5, t6);
        for (getTest test : testList) {
            final HttpResponse response = Unirest.get(baseUrl + test.path).asString();
            assertEquals(test.returnCode, response.getStatus());
            assertNotNull(response.getBody());
        }

    }

    @Test
    void postTranca_sucess() {
        Tranca b = new Tranca();
        b.setNumero(1);
        b.setLocalizacao("0,0");
        b.setAnoDeFabricacao("2020");
        b.setModelo("Modelo");
        b.setStatus(Tranca.Status.LIVRE);

        final HttpResponse response = Unirest.post(baseUrl + "/tranca").body(b).asString();
        assertEquals(201, response.getStatus());
    }

    @Test
    void postTranca_fail() {
        String body = "'{\n" +
                "  \"numero\": \"a\",\n" +
                "  \"localizacao\": \"string\",\n" +
                "  \"anoDeFabricacao\": \"string\",\n" +
                "  \"modelo\": \"string\",\n" +
                "  \"status\": \"\"\n" +
                "}'";

        final HttpResponse response = Unirest.post(baseUrl + "/tranca").body(body).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void postTranca_integrarNaRede_sucess() {
        TrancaTotem b = new TrancaTotem();
        b.setIdTranca("00000000-0000-0000-0000-000000000001");
        b.setIdTotem("00000000-0000-0000-0000-000000000020");

        final HttpResponse response = Unirest.post(baseUrl + "/tranca/integrarNaRede").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void postTranca_integrarNaRede_404tranca() {
        TrancaTotem b = new TrancaTotem();
        b.setIdTranca("00000000-0000-0000-0000-000000000009");
        b.setIdTotem("00000000-0000-0000-0000-000000000020");

        final HttpResponse response = Unirest.post(baseUrl + "/tranca/integrarNaRede").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void postTranca_integrarNaRede_404totem() {
        TrancaTotem b = new TrancaTotem();
        b.setIdTranca("00000000-0000-0000-0000-000000000001");
        b.setIdTotem("00000000-0000-0000-0000-000000000029");

        final HttpResponse response = Unirest.post(baseUrl + "/tranca/integrarNaRede").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void postTranca_retirarDaRede_sucess() {
        TrancaTotem b = new TrancaTotem();
        b.setIdTranca("00000000-0000-0000-0000-000000000002");
        b.setIdTotem("00000000-0000-0000-0000-000000000020");

        final HttpResponse response = Unirest.post(baseUrl + "/tranca/retirarDaRede").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void postTranca_retirarDaRede_fail() {
        TrancaTotem b = new TrancaTotem();
        b.setIdTranca("A");
        b.setIdTotem("00000000-0000-0000-0000-000000000020");

        final HttpResponse response = Unirest.post(baseUrl + "/tranca/retirarDaRede").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void putTranca_success() {
        Tranca b = new Tranca();
        b.setNumero(1);
        b.setLocalizacao("0,0");
        b.setAnoDeFabricacao("2020");
        b.setModelo("Modelo");
        b.setStatus(Tranca.Status.LIVRE);
        final HttpResponse response = Unirest.put(baseUrl + "/tranca/00000000-0000-0000-0000-000000000001").body(b).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void putTranca_fail_404() {
        Tranca b = new Tranca();
        b.setNumero(1);
        b.setLocalizacao("0,0");
        b.setAnoDeFabricacao("2020");
        b.setModelo("Modelo");
        b.setStatus(Tranca.Status.LIVRE);
        final HttpResponse response = Unirest.put(baseUrl + "/tranca/00000000-0000-0000-0000-000000000920").body(b).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void delTranca_success() {
        final HttpResponse response = Unirest.delete(baseUrl + "/tranca/00000000-0000-0000-0000-000000000002").asString();
        assertEquals(204, response.getStatus());
    }

    @Test
    void delTranca_fail_404() {
        final HttpResponse response = Unirest.delete(baseUrl + "/tranca/00000000-0000-0000-0000-000000000009").asString();
        assertEquals(404, response.getStatus());
    }
}