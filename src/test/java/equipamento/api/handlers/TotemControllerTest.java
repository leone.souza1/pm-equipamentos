package equipamento.api.handlers;

import equipamento.api.JavalinApp;
import equipamento.api.requests.IntegrateBicicleta;
import equipamento.core.entities.Bicicleta;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class TotemControllerTest {

    private static JavalinApp app;
    private static String baseUrl;

    @BeforeAll
    static void init() {
        int port = 7012;
        baseUrl = "http://localhost:" + port;
        app = new JavalinApp();
        app.start(port);
    }

    @AfterAll
    static void afterAll() {
        app.stop();
    }

    @Test
    void getTests() {
        class getTest {
            public final String path;
            public final int returnCode;

            getTest(String path, int code) {
                this.path = path;
                this.returnCode = code;
            }
        }
        getTest t1 = new getTest("/totem", 200);
        getTest t2 = new getTest("/totem/00000000-0000-0000-0000-000000000020/trancas", 200);
        getTest t3 = new getTest("/totem/00000000-0000-0000-0000-000000000023/trancas", 404);
        getTest t4 = new getTest("/totem/A/trancas", 422);
        getTest t5 = new getTest("/totem/00000000-0000-0000-0000-000000000020/bicicletas", 200);
        getTest t6 = new getTest("/totem/00000000-0000-0000-0000-000000000023/bicicletas", 404);
        getTest t7 = new getTest("/totem/A/bicicletas", 422);

        List<getTest> testList = Arrays.asList(t1, t2, t3, t4, t5, t6, t7);
        for (getTest test : testList) {
            final HttpResponse response = Unirest.get(baseUrl + test.path).asString();
            assertEquals(test.returnCode, response.getStatus());
            assertNotNull(response.getBody());
        }

    }

    @Test
    void postTotem_sucess() {
        String body = "{" +
                "\n\"localizacao\": \"0,0\"" +
                "\n}";
        final HttpResponse response = Unirest.post(baseUrl + "/totem").body(body).asString();
        assertEquals(201, response.getStatus());
    }

    @Test
    void postTotem_fail() {
        String body = "{" +
                "\n\"localizacao\": " +
                "\n}";
        final HttpResponse response = Unirest.post(baseUrl + "/totem").body(body).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void putTotem_success() {
        String body = "{" +
                "\n\"localizacao\": \"0,10\"" +
                "\n}";
        final HttpResponse response = Unirest.put(baseUrl + "/totem/00000000-0000-0000-0000-000000000020").body(body).asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void putTotem_fail_422() {
        String body = "{" +
                "\n\"localizacao\": " +
                "\n}";
        final HttpResponse response = Unirest.put(baseUrl + "/totem/00000000-0000-0000-0000-000000000020").body(body).asString();
        assertEquals(422, response.getStatus());
    }

    @Test
    void putTotem_fail_404() {
        String body = "{" +
                "\n\"localizacao\": \"0,10\"" +
                "\n}";
        final HttpResponse response = Unirest.put(baseUrl + "/totem/00000000-0000-0000-0000-000000000920").body(body).asString();
        assertEquals(404, response.getStatus());
    }

    @Test
    void delTotem_success() {
        System.out.println("delTotem_success");
        final HttpResponse response = Unirest.delete(baseUrl + "/totem/00000000-0000-0000-0000-000000000025").asString();
        assertEquals(200, response.getStatus());
    }

    @Test
    void delTotem_fail_404() {
        final HttpResponse response = Unirest.delete(baseUrl + "/totem/00000000-0000-0000-0000-000000000922").asString();
        assertEquals(404, response.getStatus());
    }
}