package equipamento.api.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ErrorResponse {
    private String id;
    private Integer codigo;
    private String mensagem;
}
