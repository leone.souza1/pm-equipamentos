package equipamento.api;

import equipamento.api.handlers.*;
import equipamento.core.repositories.BicicletaRepository;
import equipamento.core.repositories.TotemRepository;
import equipamento.core.repositories.TrancaRepository;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {
        private Javalin app = Javalin.create(config -> {
                config.contextPath = "/";
                config.enableDevLogging();
                config.enableCorsForAllOrigins();
                config.defaultContentType = "application/json";
        })
                        .routes(() -> {
                                // Repositories
                                BicicletaRepository bicicletaRepository = new BicicletaRepository();
                                TotemRepository totemRepository = new TotemRepository();
                                TrancaRepository trancaRepository = new TrancaRepository();

                                // Handlers
                                BicicletaController bicicletaHandler = new BicicletaController(bicicletaRepository,
                                                trancaRepository);
                                TotemController totemHandler = new TotemController(totemRepository, trancaRepository,
                                                bicicletaRepository);
                                TrancaController trancaHandler = new TrancaController(trancaRepository,
                                                bicicletaRepository, totemRepository);

                                get("/healthcheck", Controller::healthCheck);

                                path("/bicicleta", () -> {
                                        get("/", bicicletaHandler::listar);
                                        post("/", bicicletaHandler::cadastrarBicicleta);
                                        post("/integrarNaRede", bicicletaHandler::integrarNaRede);
                                        post("/retirarDaRede", bicicletaHandler::retirarDaRede);
                                        get("/{idBicicleta}", context -> bicicletaHandler.obterBicicleta(context,
                                                        context.pathParam("idBicicleta")));
                                        put("/{idBicicleta}", context -> bicicletaHandler.editarBicicleta(context,
                                                        context.pathParam("idBicicleta")));
                                        delete("/{idBicicleta}", context -> bicicletaHandler.removerBicicleta(context,
                                                        context.pathParam("idBicicleta")));
                                        post("/{idBicicleta}/status/{acao}",
                                                        context -> bicicletaHandler.alterarStatusBicicleta(context,
                                                                        context.pathParam("idBicicleta"),
                                                                        context.pathParam("acao")));
                                });

                                path("/totem", () -> {
                                        get("/", totemHandler::obterTotens);
                                        post("/", totemHandler::cadastrarTotem);
                                        put("/{idTotem}", context -> totemHandler.editarTotem(context,
                                                        context.pathParam("idTotem")));
                                        delete("/{idTotem}", context -> totemHandler.removerTotem(context,
                                                        context.pathParam("idTotem")));
                                        get("/{idTotem}/trancas", context -> totemHandler.obterTrancasTotem(context,
                                                        context.pathParam("idTotem")));
                                        get("/{idTotem}/bicicletas",
                                                        context -> totemHandler.obterBicicletasTotem(context,
                                                                        context.pathParam("idTotem")));
                                });

                                path("/tranca", () -> {
                                        get("/", trancaHandler::listar);
                                        post("/", trancaHandler::cadastrar);
                                        post("/integrarNaRede", trancaHandler::integrarNaRede);
                                        post("/retirarDaRede", trancaHandler::retirarDaRede);
                                        get("/{idTranca}", context -> trancaHandler.obterTranca(context,
                                                        context.pathParam("idTranca")));
                                        get("/{idTranca}/bicicleta",
                                                        context -> trancaHandler.obterBicicletaNaTranca(context,
                                                                        context.pathParam("idTranca")));
                                        put("/{idTranca}", context -> trancaHandler.editarTranca(context,
                                                        context.pathParam("idTranca")));
                                        delete("/{idTranca}", context -> trancaHandler.removerTranca(context,
                                                        context.pathParam("idTranca")));
                                        post("/{idTranca}/status/{acao}",
                                                        context -> trancaHandler.alterarStatusTranca(context,
                                                                        context.pathParam("idTranca"),
                                                                        context.pathParam("acao")));
                                });

                        });

        public void start(int port) {
                this.app.start(port);
        }

        public void stop() {
                this.app.stop();
        }
}
