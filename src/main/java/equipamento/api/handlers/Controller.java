package equipamento.api.handlers;

import io.javalin.http.Context;

public class Controller {

    private Controller(){}

    public static void healthCheck(Context context) {
        context.status(200);
        context.result("Status OK");
    }
}
