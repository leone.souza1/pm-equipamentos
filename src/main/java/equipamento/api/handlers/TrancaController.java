package equipamento.api.handlers;

import equipamento.api.requests.TrancaTotem;
import equipamento.core.entities.Bicicleta;
import equipamento.core.entities.Totem;
import equipamento.core.entities.Tranca;
import equipamento.core.repositories.BicicletaRepository;
import equipamento.core.repositories.TotemRepository;
import equipamento.core.repositories.TrancaRepository;

import io.javalin.http.Context;

import java.util.Optional;
import java.util.UUID;

import static equipamento.core.utils.ProvideErrorResponse.provideErrorResponse;

public class TrancaController {

    private final TrancaRepository trancaRepository;
    private final BicicletaRepository bicicletaRepository;
    private final TotemRepository totemRepository;

    public TrancaController(TrancaRepository trancaRepository, BicicletaRepository bicicletaRepository, TotemRepository totemRepository) {
        this.trancaRepository = trancaRepository;
        this.bicicletaRepository = bicicletaRepository;
        this.totemRepository = totemRepository;
    }

    public void listar(Context context) {
        context.json(trancaRepository.findAll());
    }

    public void cadastrar(Context context) {
        try {
            Tranca tranca = context.bodyAsClass(Tranca.class);
            if (tranca == null){
                provideErrorResponse(context, "", 422, "Dados inválidos");
                return;
            }
            tranca.setId(UUID.randomUUID());
            trancaRepository.newTranca(tranca);
            context.status(201);
            context.json(tranca);
        } catch (Exception ignored) {
            provideErrorResponse(context, "", 422, "Dados inválidos");
        }
    }

    public void integrarNaRede(Context context) {
        TrancaTotem trancaTotem = context.bodyAsClass(TrancaTotem.class);
        Tranca tranca = getTrancaByIdTranca(context, trancaTotem.getIdTranca());
        Totem totem = TotemController.getTotemByIdTotem(context, trancaTotem.getIdTotem(), totemRepository);
        if (tranca == null || totem == null) {
            return;
        }
        tranca.setLocalizacao(totem.getLocalizacao());
    }

    public void retirarDaRede(Context context) {
        //set location as null for Tranca
        TrancaTotem trancaTotem = context.bodyAsClass(TrancaTotem.class);
        Tranca tranca = getTrancaByIdTranca(context, trancaTotem.getIdTranca());
        if (tranca == null){
            return;
        }

        tranca.setLocalizacao(null);
        tranca.setBicicleta(null);
    }

    public void obterTranca(Context context, String idTranca) {
        Tranca tranca = getTrancaByIdTranca(context, idTranca);
        if (tranca == null){
            return;
        }


        context.json(tranca);
        context.status(200);
    }

    public void obterBicicletaNaTranca(Context context, String idTranca) {
        Optional<Tranca> tranca = obterTrancaObject(idTranca);
        if (tranca.isPresent()) {
            UUID uuidBicicleta = tranca.get().getBicicleta();
            if (uuidBicicleta == null) {
                provideErrorResponse(context, idTranca, 404, "Bicicleta não encontrada");
                return;
            }
            Optional<Bicicleta> bicicleta = bicicletaRepository.findById(uuidBicicleta);
            if (bicicleta.isPresent()) {
                context.json(bicicleta.get());
                context.status(200);
                return;
            }
            provideErrorResponse(context, idTranca, 404, "Bicicleta não encontrada");
        }
        provideErrorResponse(context, idTranca, 404, "Tranca não encontrada");
    }

    private Optional<Tranca> obterTrancaObject(String idTranca) {
        try {
            UUID uuid = UUID.fromString(idTranca);
            return trancaRepository.findById(uuid);
        } catch (Exception ignored) {
        }

        return Optional.empty();
    }

    public void editarTranca(Context context, String idTranca) {
        Tranca newTranca = trancaFromAPI(context, idTranca);
        if (newTranca == null) {
            return;
        }
        Tranca tranca = getTrancaByIdTranca(context, idTranca);
        if (tranca == null) {
            return;
        }
        trancaRepository.save(newTranca);
        context.json(newTranca);
        context.status(200);
    }

    public void removerTranca(Context context, String idTranca) {
        Tranca tranca = getTrancaByIdTranca(context, idTranca);
        if (tranca == null) {
            return;
        }

        UUID uuidTranca = UUID.fromString(idTranca);
        trancaRepository.delete(uuidTranca);
        context.status(204);
    }

    public void alterarStatusTranca(Context context, String idTranca, String status) {
        Tranca tranca = getTrancaByIdTranca(context, idTranca);
        if (tranca == null) {
            return;
        }

        try {
            tranca.setStatus(Tranca.Status.valueOf(status));
        } catch (Exception ignored) {
            provideErrorResponse(context, idTranca, 422, "Status inválido");
            return;
        }
        trancaRepository.save(tranca);
        context.json(tranca);
        context.status(200);

    }
    public Tranca getTrancaByIdTranca(Context context, String idTranca) {
        UUID uuid;
        try {
            uuid = UUID.fromString(idTranca);
        } catch (Exception ignored) {
            provideErrorResponse(context, idTranca, 404, "Tranca não encontrada");
            return null;
        }
        if (!trancaRepository.findById(uuid).isPresent()) {
            provideErrorResponse(context, uuid.toString(), 404, "Tranca não encontrada");
            return null;
        }
        return trancaRepository.findById(uuid).get();
    }

    private Tranca trancaFromAPI(Context context, String idTranca) {
        try {
            Tranca tranca = context.bodyAsClass(Tranca.class);
            tranca.setId(UUID.fromString(idTranca));
            return tranca;
        } catch (Exception exception) {
            provideErrorResponse(context, idTranca, 422, "Dados Inválidos");
            return null;
        }
    }
}
