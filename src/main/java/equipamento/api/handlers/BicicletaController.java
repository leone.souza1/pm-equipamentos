package equipamento.api.handlers;

import equipamento.core.entities.Bicicleta;
import equipamento.core.entities.Tranca;
import equipamento.core.repositories.BicicletaRepository;
import equipamento.core.repositories.TrancaRepository;
import equipamento.api.requests.IntegrateBicicleta;
import io.javalin.http.Context;

import java.util.UUID;

import static equipamento.core.utils.ProvideErrorResponse.provideErrorResponse;

public class BicicletaController {

    private final BicicletaRepository bicicletas;
    private final TrancaRepository trancas;

    public BicicletaController(BicicletaRepository bicicletas, TrancaRepository trancas) {
        this.bicicletas = bicicletas;
        this.trancas = trancas;
    }

    public void listar(Context context) {
        context.json(bicicletas.findAll());
    }

    public void cadastrarBicicleta(Context context) {
        UUID uuid = UUID.randomUUID();
        Bicicleta bicicleta = bicicletaFromAPI(context, uuid);
        if (bicicleta == null) {
            return;
        }

        bicicletas.save(bicicleta);
        context.json(bicicleta);
        context.status(201);
    }

    public void integrarNaRede(Context context) {
        IntegrateBicicleta request = context.bodyAsClass(IntegrateBicicleta.class);
        try {
            UUID uuidTranca = UUID.fromString(request.getIdTranca());
            Tranca tranca = trancas.findById(uuidTranca).orElse(null);
            if (tranca == null) {
                provideErrorResponse(context, request.getIdTranca(), 404, "Tranca não encontrada");
                return;
            }
            if (tranca.getStatus() != Tranca.Status.LIVRE && tranca.getStatus() != Tranca.Status.NOVA) {
                provideErrorResponse(context, request.getIdTranca(), 422, "Tranca não está disponível");
                return;
            }

            Bicicleta bicicleta = getBicicletaByIdBicicleta(context, request.getIdBicicleta());
            if (bicicleta == null) {
                return;
            }
            if (bicicleta.getStatus() != Bicicleta.Status.DISPONIVEL
                    && bicicleta.getStatus() != Bicicleta.Status.NOVA) {
                provideErrorResponse(context, request.getIdBicicleta(), 422, "Bicicleta não está disponível");
                return;
            }

            trancas.integrateBicicleta(tranca, bicicleta);
            bicicletas.setStatus(bicicleta, Bicicleta.Status.EM_USO);
        } catch (Exception ignored) {
            provideErrorResponse(context, request.getIdBicicleta(), 422, "Erro ao integrar bicicleta na rede");
        }
    }

    public void retirarDaRede(Context context) {
        IntegrateBicicleta request = context.bodyAsClass(IntegrateBicicleta.class);
        try {
            UUID uuidTranca = UUID.fromString(request.getIdTranca());
            Tranca tranca = trancas.findById(uuidTranca).orElse(null);
            if (tranca == null) {
                provideErrorResponse(context, request.getIdTranca(), 404, "Tranca não encontrada");
                return;
            }
            if (tranca.getStatus() == Tranca.Status.LIVRE || tranca.getStatus() == Tranca.Status.NOVA) {
                provideErrorResponse(context, request.getIdTranca(), 422, "Tranca não está em uso");
                return;
            }

            Bicicleta bicicleta = getBicicletaByIdBicicleta(context, request.getIdBicicleta());
            if (bicicleta == null) {
                return;
            }

            if (bicicleta.getStatus() == Bicicleta.Status.DISPONIVEL
                    || bicicleta.getStatus() == Bicicleta.Status.NOVA) {
                provideErrorResponse(context, request.getIdBicicleta(), 422, "Bicicleta não está em uso");
                return;
            }

            trancas.removeBicicleta(tranca);
            bicicletas.setStatus(bicicleta, Bicicleta.Status.DISPONIVEL);
        } catch (Exception ignored) {
            provideErrorResponse(context, request.getIdBicicleta(), 422, "Erro ao integrar bicicleta na rede");
        }
    }

    public void obterBicicleta(Context context, String idBicicleta) {
        Bicicleta bicicleta = getBicicletaByIdBicicleta(context, idBicicleta);
        if (bicicleta == null) {
            return;
        }

        context.json(bicicleta);
        context.status(200);
    }

    public void editarBicicleta(Context context, String idBicicleta) {
        UUID uuid = UUID.fromString(idBicicleta);
        Bicicleta newBicicleta = bicicletaFromAPI(context, uuid);
        if (newBicicleta == null) {
            return;
        }

        Bicicleta bicicleta = getBicicletaByIdBicicleta(context, idBicicleta);
        if (bicicleta == null) {
            return;
        }
        bicicletas.save(newBicicleta);
        context.json(newBicicleta);
        context.status(200);
    }

    public void removerBicicleta(Context context, String idBicicleta) {
        Bicicleta bicicleta = getBicicletaByIdBicicleta(context, idBicicleta);
        if (bicicleta == null) {
            return;
        }

        UUID uuid = UUID.fromString(idBicicleta);
        bicicletas.delete(uuid);
        context.status(204);
    }

    public void alterarStatusBicicleta(Context context, String idBicicleta, String acao) {
        Bicicleta bicicleta = getBicicletaByIdBicicleta(context, idBicicleta);
        if (bicicleta == null) {
            return;
        }

        try {
            bicicleta.setStatus(Bicicleta.Status.valueOf(acao));
        } catch (Exception ignored) {
            provideErrorResponse(context, idBicicleta, 422, "Status inválido");
            return;
        }
        bicicletas.save(bicicleta);
        context.json(bicicleta);
        context.status(200);
    }

    public Bicicleta getBicicletaByIdBicicleta(Context context, String idBicicleta) {
        UUID uuid;
        try {
            uuid = UUID.fromString(idBicicleta);
        } catch (Exception ignored) {
            provideErrorResponse(context, idBicicleta, 404, "Bicicleta não encontrada");
            return null;
        }
        if (!this.bicicletas.findById(uuid).isPresent()) {
            provideErrorResponse(context, uuid.toString(), 404, "Bicicleta não encontrada");
            return null;
        }
        return this.bicicletas.findById(uuid).get();
    }

    private Bicicleta bicicletaFromAPI(Context context, UUID uuid) {
        try {
            Bicicleta bicicleta = context.bodyAsClass(Bicicleta.class);
            bicicleta.setId(uuid);
            return bicicleta;
        } catch (Exception exception) {
            provideErrorResponse(context, uuid.toString(), 422, "Dados Inválidos");
            return null;
        }
    }
}
