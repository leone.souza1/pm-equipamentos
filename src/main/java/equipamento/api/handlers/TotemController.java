package equipamento.api.handlers;

import equipamento.core.entities.Bicicleta;
import equipamento.core.entities.Totem;
import equipamento.core.entities.Tranca;
import equipamento.core.repositories.BicicletaRepository;
import equipamento.core.repositories.TotemRepository;
import equipamento.core.repositories.TrancaRepository;
import io.javalin.http.Context;

import java.util.*;

import static equipamento.core.utils.ProvideErrorResponse.provideErrorResponse;

public class TotemController {

    private final TotemRepository totemRepository;
    private final TrancaRepository trancaRepository;
    private final BicicletaRepository bicicletaRepository;

    public TotemController(TotemRepository totemRepository, TrancaRepository trancaRepository, BicicletaRepository bicicletaRepository) {
        this.totemRepository = totemRepository;
        this.trancaRepository = trancaRepository;
        this.bicicletaRepository = bicicletaRepository;
    }

    public static Totem getTotemByIdTotem(Context context, String idTotem, TotemRepository totemRepository) {
        UUID uuid;
        try {
            uuid = UUID.fromString(idTotem);
        } catch (Exception ignored) {
            provideErrorResponse(context, idTotem, 404, "Totem não encontrado");
            return null;
        }
        if (!totemRepository.getTotemById(uuid).isPresent()) {
            provideErrorResponse(context, uuid.toString(), 404, "Totem não encontrado");
            return null;
        }
        return totemRepository.getTotemById(uuid).get();
    }

    public void obterTotens(Context context) {
        context.json(totemRepository.findAll());
    }

    public void cadastrarTotem(Context context) {
        try {
            Totem totem = context.bodyAsClass(Totem.class);
            if (totem == null){
                provideErrorResponse(context, "", 422, "Dados inválidos");
                return;
            }
            totem.setId(UUID.randomUUID());
            totemRepository.newTotem(totem);
            context.status(201);
            context.json(totem);
        } catch (Exception ignored) {
            provideErrorResponse(context, "", 422, "Dados inválidos");
            //TODO: Check field by field if it's valid
        }
    }

    public void editarTotem(Context context, String idTotem) {
        try {
            Totem newTotem = context.bodyAsClass(Totem.class);
            if (newTotem == null){
                provideErrorResponse(context, "", 422, "Dados inválidos");
                return;
            }

            Totem totem = getTotemByUuid(context, idTotem);
            if (totem == null){
                return;
            }

            totem.setId(UUID.fromString(idTotem));
            totemRepository.save(totem);
            context.json(totem);
            context.status(200);

        } catch (Exception ignored) {
            provideErrorResponse(context, "", 422, "Dados inválidos");
            //TODO: Check field by field if it's valid
        }
    }

    public void removerTotem(Context context, String totemId) {
        UUID uuid;
        try {
            uuid = UUID.fromString(totemId);
        } catch (IllegalArgumentException e) {
            provideErrorResponse(context, totemId, 404, "Totem não encontrado");
            return;
        }
        if (!totemRepository.getTotemById(uuid).isPresent()) {
            provideErrorResponse(context, totemId, 404, "Totem não encontrado");
            return;
        }
        totemRepository.delete(uuid);
    }

    public void obterTrancasTotem(Context context, String idTotem) {
        Totem totem = getTotemByUuid(context, idTotem);
        if (totem == null) {
            return;
        }

        String localizacao = totem.getLocalizacao();

        context.json(trancaRepository.findAllByLocalizacao(localizacao));
        context.status(200);

    }

    public void obterBicicletasTotem(Context context, String idTotem) {
        Totem totem = getTotemByUuid(context, idTotem);
        if (totem == null) {
            return;
        }

        String localizacao = totem.getLocalizacao();

        List<Tranca> trancas = trancaRepository.findAllByLocalizacao(localizacao);
        List<UUID> bicicletasId = new ArrayList<>();
        for (Tranca tranca : trancas) {
            bicicletasId.add(tranca.getBicicleta());
        }

        List<Bicicleta> bicicletas = new ArrayList<>();
        for (UUID bicicletaId : bicicletasId) {
            if (bicicletaId == null) {
                continue;
            }
            Optional<Bicicleta> bicicletaOptional = bicicletaRepository.findById(bicicletaId);
            bicicletaOptional.ifPresent(bicicletas::add);
        }

        context.json(bicicletas);
        context.status(200);
    }

    private Totem getTotemByUuid(Context context, String idTotem) {
        UUID uuid;
        try {
            uuid = UUID.fromString(idTotem);
        } catch (IllegalArgumentException e) {
            provideErrorResponse(context, idTotem, 422, "Dados inválidos");
            return null;
        }
        if (!totemRepository.getTotemById(uuid).isPresent()) {
            provideErrorResponse(context, idTotem, 404, "Totem não encontrado");
            return null;
        }
        return totemRepository.getTotemById(uuid).get();
    }
}
