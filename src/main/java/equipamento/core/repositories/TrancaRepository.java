package equipamento.core.repositories;

import equipamento.core.entities.Bicicleta;
import equipamento.core.entities.Tranca;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class TrancaRepository {
    private final Map<UUID, Tranca> trancas;

    public TrancaRepository() {
        this.trancas = new ConcurrentHashMap<>();

        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000000"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000000"), null, 1, "0,0", "2020", "Modelo", Tranca.Status.LIVRE));
        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000001"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000001"), UUID.fromString("00000000-0000-0000-0000-000000000011"), 2, "0,0", "2020", "Modelo1", Tranca.Status.OCUPADA));
        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000002"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000002"), null, 3, "0,1", "2020", "Modelo2", Tranca.Status.APOSENTADA));
        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000003"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000003"), UUID.fromString("00000000-0000-0000-0000-000000000012"), 2, "0,0", "2020", "Modelo3", Tranca.Status.OCUPADA));
        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000004"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000004"), UUID.fromString("00000000-0000-0000-0000-000000000013"), 2, "0,0", "2020", "Modelo4", Tranca.Status.LIVRE));
        this.trancas.put(UUID.fromString("00000000-0000-0000-0000-000000000005"), new Tranca(UUID.fromString("00000000-0000-0000-0000-000000000005"), UUID.fromString("00000000-0000-0000-0000-000000000015"), 2, "0,0", "2020", "Modelo5", Tranca.Status.OCUPADA));


    }

    public Collection<Tranca> findAll() {
        return this.trancas.values();
    }

    public List<Tranca> findAllByLocalizacao(String localizacao) {
        List<Tranca> trancas = new ArrayList<>();
        for (Tranca tranca : this.trancas.values()) {
            if (tranca.getLocalizacao().equals(localizacao)) {
                trancas.add(tranca);
            }
        }
        return trancas;
    }

    public Optional<Tranca> findById(UUID uuid) {
        return Optional.ofNullable(this.trancas.get(uuid));
    }

    public void integrateBicicleta(Tranca tranca, Bicicleta bicicleta) {
        tranca.setBicicleta(bicicleta.getId());
        tranca.setStatus(Tranca.Status.OCUPADA);
        this.trancas.replace(tranca.getId(), tranca);
    }

    public void removeBicicleta(Tranca tranca) {
        tranca.setBicicleta(null);
        tranca.setStatus(Tranca.Status.LIVRE);
        this.trancas.replace(tranca.getId(), tranca);
    }

    public void newTranca(Tranca tranca) {
        tranca.setId(UUID.randomUUID());
        this.trancas.put(tranca.getId(), tranca);
    }

    public void delete(UUID uuid) {
        this.trancas.remove(uuid);
    }

    public void save(Tranca tranca) {
        this.trancas.put(tranca.getId(), tranca);
    }
}
