package equipamento.core.repositories;

import equipamento.core.entities.Totem;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class TotemRepository {
    private final Map<UUID, Totem> totems;

    public TotemRepository() {
        this.totems = new ConcurrentHashMap<>();

        this.totems.put(UUID.fromString("00000000-0000-0000-0000-000000000020"), new Totem(UUID.fromString("00000000-0000-0000-0000-000000000020"), "0,0"));
        this.totems.put(UUID.fromString("00000000-0000-0000-0000-000000000021"), new Totem(UUID.fromString("00000000-0000-0000-0000-000000000021"), "0,1"));
        this.totems.put(UUID.fromString("00000000-0000-0000-0000-000000000022"), new Totem(UUID.fromString("00000000-0000-0000-0000-000000000022"), "1,2"));
        this.totems.put(UUID.fromString("00000000-0000-0000-0000-000000000025"), new Totem(UUID.fromString("00000000-0000-0000-0000-000000000025"), "1,2"));
    }

    public Collection<Totem> findAll() {
        return this.totems.values();
    }

    public void delete(UUID totemId) {
        this.totems.remove(totemId);
    }

    public Optional<Totem> getTotemById(UUID id) {
        return this.totems.values().stream()
            .filter(bicicleta -> bicicleta.getId().equals(id))
            .findFirst();
    }

    public void newBicicleta(Totem totem) {
        totem.setId(UUID.randomUUID());
        this.totems.put(totem.getId(), totem);
    }

    public void updateBicicleta(Totem totem) {
        this.totems.put(totem.getId(), totem);
    }

    public void newTotem(Totem totem) {
        this.totems.put(totem.getId(), totem);
    }

    public void save(Totem totem) {
        this.totems.put(totem.getId(), totem);
    }
}
