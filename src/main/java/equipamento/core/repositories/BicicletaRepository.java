package equipamento.core.repositories;

import equipamento.core.entities.Bicicleta;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class BicicletaRepository {
    private final Map<UUID, Bicicleta> bicicletas;

    public BicicletaRepository() {
        this.bicicletas = new ConcurrentHashMap<>();
        Bicicleta b1 = new Bicicleta(UUID.fromString("00000000-0000-0000-0000-000000000010"), "marca1", "modelo1", "ano1", 1, Bicicleta.Status.DISPONIVEL);
        Bicicleta b2 = new Bicicleta(UUID.fromString("00000000-0000-0000-0000-000000000011"), "marca2", "modelo2", "ano2", 2, Bicicleta.Status.EM_USO);
        Bicicleta b3 = new Bicicleta(UUID.fromString("00000000-0000-0000-0000-000000000012"), "marca3", "modelo3", "ano3", 3, Bicicleta.Status.DISPONIVEL);
        Bicicleta b4 = new Bicicleta(UUID.fromString("00000000-0000-0000-0000-000000000015"), "marca3", "modelo3", "ano3", 3, Bicicleta.Status.EM_USO);

        this.bicicletas.put(b1.getId(), b1);
        this.bicicletas.put(b2.getId(), b2);
        this.bicicletas.put(b3.getId(), b3);
        this.bicicletas.put(b4.getId(), b4);
    }

    public Collection<Bicicleta> findAll() {
        return this.bicicletas.values();
    }

    public Optional<Bicicleta> findById(UUID uuid) {
        return Optional.ofNullable(this.bicicletas.get(uuid));
    }

    public void save(Bicicleta bicicleta) {
        this.bicicletas.put(bicicleta.getId(), bicicleta);
    }

    public void delete(UUID uuid) {
        this.bicicletas.remove(uuid);
    }

    public void setStatus(Bicicleta bicicleta, Bicicleta.Status status) {
        bicicleta.setStatus(status);
        this.bicicletas.replace(bicicleta.getId(), bicicleta);
    }
}
