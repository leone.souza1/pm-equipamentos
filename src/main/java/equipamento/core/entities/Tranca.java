package equipamento.core.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Tranca {
        private UUID id;
        private UUID bicicleta;
        private Integer numero;
        private String localizacao;
        private String anoDeFabricacao;
        private String modelo;
        private Status status;

        public enum Status {
            LIVRE,
            OCUPADA,
            NOVA,
            APOSENTADA,
            EM_REPARO
        }
}

