package equipamento.core.entities;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Bicicleta {
    private UUID id;
    private String marca;
    private String modelo;
    private String ano;
    private Integer numero;
    private Status status;

    public enum Status {
        DISPONIVEL,
        EM_USO,
        NOVA,
        APOSENTADA,
        REPARO_SOLICITADO,
        EM_REPARO
    }
}

