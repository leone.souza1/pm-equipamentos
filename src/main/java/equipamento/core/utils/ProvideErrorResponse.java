package equipamento.core.utils;

import io.javalin.http.Context;

public class ProvideErrorResponse {
    public static void provideErrorResponse(Context context, String id, int statusCode, String message) {
        equipamento.api.responses.ErrorResponse err = new equipamento.api.responses.ErrorResponse(id, statusCode, message);
        context.json(err);
        context.status(statusCode);
    }
}
